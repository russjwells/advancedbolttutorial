﻿using UnityEngine;
using System.Collections;

public class TutorialPlayerController : Bolt.EntityBehaviour<ITutorialPlayerState> {

	const float MOUSE_SENSITIVITY = 2f;

	bool _forward;
	bool _backward;
	bool _left;
	bool _right;
	bool _jump;

	float _yaw;
	float _pitch;

	PlayerMotor _motor;

	void Awake() {
		_motor = GetComponent<PlayerMotor>();
	}

	public override void Attached() {
		state.Transform.SetTransforms(transform);
	}

	void PollKeys(bool mouse) {
		_forward = Input.GetKey(KeyCode.W);
		_backward = Input.GetKey(KeyCode.S);
		_left = Input.GetKey(KeyCode.A);
		_right = Input.GetKey(KeyCode.D);
		_jump = Input.GetKey(KeyCode.Space);

		if (mouse) {
			_yaw += (Input.GetAxisRaw("Mouse X") * MOUSE_SENSITIVITY);
			_yaw %= 360f;

			_pitch += (-Input.GetAxisRaw("Mouse Y") * MOUSE_SENSITIVITY);
			_pitch = Mathf.Clamp(_pitch, -85f, +85f);
		}
	}

	void Update() {
		PollKeys(true);
	}

	public override void SimulateController() {
		PollKeys(false);

		ITutorialPlayerCommandInput input = TutorialPlayerCommand.Create();

		input.Forward = _forward;
		input.Backward = _backward;
		input.Left = _left;
		input.Right = _right;
		input.Jump = _jump;
		input.Yaw = _yaw;
		input.Pitch = _pitch;

		entity.QueueInput(input);
	}

	public override void ExecuteCommand(Bolt.Command command, bool resetState) {
		TutorialPlayerCommand cmd = (TutorialPlayerCommand)command;

		if (resetState) {
			//we got a correction!! time to reset: (runs on client only)
			_motor.SetState(cmd.Result.Position, cmd.Result.Velocity, cmd.Result.IsGrounded, cmd.Result.JumpFrames);
		}
		else {
			//apply movement (runs on serber amd cloiemnt)
			PlayerMotor.State motorState = _motor.Move(cmd.Input.Forward, cmd.Input.Backward, cmd.Input.Left, cmd.Input.Right, cmd.Input.Jump, cmd.Input.Yaw);

			// copy the motor state to the commands result (this gets sent back to the client)
			cmd.Result.Position = motorState.position;
			cmd.Result.Velocity = motorState.velocity;
			cmd.Result.IsGrounded = motorState.isGrounded;
			cmd.Result.JumpFrames = motorState.jumpFrames;
		
		}
	}

}
