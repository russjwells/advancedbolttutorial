﻿using UnityEngine;
using System.Collections.Generic;

public class TutorialPlayerObject {

	public BoltEntity character;
	public BoltConnection connection;

	public bool isServer {
		get { return connection == null; }
	}

	public bool isClient {
		get { return connection != null; }
	}

	public void Spawn() {
		if (!character) {
			character = BoltNetwork.Instantiate(BoltPrefabs.TutorialPlayer);

			if (isServer) {
				character.TakeControl();
				} else {
					character.AssignControl(connection);
				}
			}

		// teleport entity to random spawn
		character.transform.position = RandomPosition();
	}
	

	Vector3 RandomPosition() {
		float x = Random.Range(-32f, +32f);
		float z = Random.Range(-32f, +32f);
		return new Vector3(x, 32f, z);
	}
}
