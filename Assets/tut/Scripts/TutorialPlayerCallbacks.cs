﻿using UnityEngine;

[BoltGlobalBehaviour("Level2")]
public class TutorialPlayerCallbacks : Bolt.GlobalEventListener {
  public override void SceneLoadLocalDone(string map) {
    // this just instantiates our player camera, 
    // the Instantiate() method is supplied by the BoltSingletonPrefab<T> class
    PlayerCamera.Instantiate();
  }

   public override void ControlOfEntityGained(BoltEntity arg) {
    	//player camera, look at the entity
    	PlayerCamera.instance.SetTarget(arg);
    }
}