﻿using UnityEngine;

[BoltGlobalBehaviour(BoltNetworkModes.Server, "Level2")]
public class TutorialServerCallBacks : Bolt.GlobalEventListener {

	void Awake() {
		TutorialPlayerObjectRegistry.CreateServerPlayer();
	}

	public override void Connected(BoltConnection arg) {
		TutorialPlayerObjectRegistry.CreateClientPlayer(arg);
	}

	//when the servers scene is loaded...
	public override void SceneLoadLocalDone(string map) {
		TutorialPlayerObjectRegistry.serverPlayer.Spawn();
	}

	//when a client's scene is loaded...
	public override void SceneLoadRemoteDone(BoltConnection connection){
		TutorialPlayerObjectRegistry.GetTutorialPlayer(connection).Spawn();
	}


}